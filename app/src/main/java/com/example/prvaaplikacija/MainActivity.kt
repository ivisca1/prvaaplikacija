package com.example.prvaaplikacija

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button = findViewById<Button>(R.id.button1)
        button.setOnClickListener {
            showMessage();
        }
    }

    private fun showMessage(){
        val edittext = findViewById<EditText>(R.id.edittext1)
        val textview = findViewById<TextView>(R.id.textview1)
        val message = edittext.text.toString()
        Log.v("TESTLOG",message)
        textview.text = message
    }
}